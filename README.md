# R OpenBLAS CI

Test repository to experiment with downgrading the OpenBLAS the GitLab CI Docker image uses down to 0.3.3, and see if it'll fix the `p_thread22()` error.